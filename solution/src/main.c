/* main.c */

#include "./../include/bmp.h"       // NOLINT
#include "./../include/image.h"     // NOLINT
#include "./../include/rotate.h"    // NOLINT


int main(int argc, char **argv) {
    (void) argc;
    (void) argv; // supress 'unused parameters' warning

    char * input_path = argv[1];
    char * output_path = argv[2];

    struct image image;
    struct image rotated_image;

    struct bmp_header header;

    FILE *input_file;
    FILE *output_file;

    input_file = fopen(input_path, "rb");
    output_file = fopen(output_path, "w");

    if(NULL == input_file){
        printf("file do not exists\n");
        return 1;
    }
    header = read_bmp_header(input_file);
    image = read_image_data(input_file, header);
    rotated_image = rotate(image);


    return write_image(output_file, rotated_image);

}
