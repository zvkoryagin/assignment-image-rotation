//
// Created by Корягин Захар on 30.10.2022.
//
#include <stdio.h>                      // NOLINT
#include "./../include/structs.h"       // NOLINT

struct bmp_header read_bmp_header(FILE* image_file){
    struct bmp_header header = {
            .bfType = 0,
            .bfileSize = 0,
            .bfReserved = 0,
            .bOffBits = 0,
            .biSize = 0,
            .biWidth = 0,
            .biHeight = 0,
            .biPlanes = 0,
            .biBitCount = 0,
            .biCompression = 0,
            .biSizeImage = 0,
            .biXPixelsPerMeter = 0,
            .biYPixelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    fread(&header, sizeof (header), 1, image_file);
    return header;
}

struct bmp_header create_header(struct image const* image){
    struct bmp_header header = {
            .bfType = 0x4D42,
            .bfileSize = (image->width) * (image->height) * 3 + (image->height) * image->width%4+ sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = 0,
            .biXPixelsPerMeter = 0,
            .biYPixelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0};
    header.biSizeImage = header.bfileSize - header.bOffBits;
    return header;
}
