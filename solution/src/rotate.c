//
// Created by Корягин Захар on 30.10.2022.
//
//
#include "./../include/bmp.h" // NOLINT
#include <stdlib.h>           // NOLINT


struct image rotate(struct image image){
    struct image rotated_image = {
        .width = image.height,
        .height = image.width,
        .data =  (struct pixel*) malloc(image.width*image.height*3)
    };
    for (int row = 0; row < image.height; row++) {
        for (int column = 0; column <image.width; column++) {
            rotated_image.data[image.height * image.width-(row+1)-column*image.height] = image.data[(row+1)*image.width-column];
        }
    }
    free(image.data); //NOLINT
    return rotated_image;
}
