//
// Created by Корягин Захар on 30.10.2022.
//
#include "./../include/bmp.h" // NOLINT
#include <stdio.h>            // NOLINT
#include <stdlib.h>           // NOLINT
#include <stdint.h>           // NOLINT
struct image read_image_data(FILE* image_file, struct bmp_header header){
    struct image image = {
            .width = header.biWidth,
            .height = header.biHeight,
            .data = (struct pixel*) malloc(header.biSizeImage*3)
    };
    uint64_t padding = image.width%4;
    uint8_t *trash_box = malloc(padding);
    for (int column = 0; column < image.height; ++column){
        for (int row = 0; row < image.width; ++row) {
            struct pixel pixel = {
                    .r = 0,
                    .g = 0,
                    .b = 0
            };
            fread(&pixel, 3, 1, image_file);
            image.data[1+ image.width * column + row] = pixel;
        }
        fread(trash_box, 1, padding, image_file);
    }
    free(trash_box); //NOLINT
    return image;
}

int write_image(FILE* image_file, struct image image) {
    struct bmp_header new_header = create_header(&image);
    uint64_t padding = image.width%4;
    fwrite(&new_header, sizeof (new_header), 1, image_file);
    uint8_t *trash_box = malloc(padding);
    for (int i = 0; i < padding; ++i){
        trash_box[i] = 0;
    }
    for (int column = 0; column < image.height; ++column) { //NOLINT
        for (int row = 0; row < image.width; ++row) {
            struct pixel pixel = image.data[image.width * column + row];
            fwrite(&pixel, 3, 1, image_file);
        }
        fwrite(trash_box, 1, padding, image_file);
    }
    free(trash_box); //NOLINT
    free(image.data); //NOLINT
    return 0;
}

