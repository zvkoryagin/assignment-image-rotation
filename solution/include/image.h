//
// Created by Корягин Захар on 30.10.2022.
//

#ifndef IMAGE_H
#define IMAGE_H

#include "structs.h" // NOLINT

struct image read_image_data(FILE* image_file, struct bmp_header header);
int write_image(FILE* image_file, struct image image);

#endif //IMAGE_H
