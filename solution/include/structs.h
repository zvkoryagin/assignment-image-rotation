//
// Created by Корягин Захар on 30.10.2022.
//

#ifndef STRUCTS_H
#define STRUCTS_H
#include <stdlib.h> // NOLINT
#include <stdint.h> // NOLINT


struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPixelsPerMeter;
    uint32_t biYPixelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct pixel { uint8_t r, g, b; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};
#endif //STRUCTS_H
