//
// Created by Корягин Захар on 30.10.2022.
//

#ifndef BMP_H
#define BMP_H

#include "./structs.h" // NOLINT
#include <stdio.h>   // NOLINT

struct bmp_header read_bmp_header(FILE* image_file);
struct bmp_header create_header(struct image const* image);

#endif //BMP_H
