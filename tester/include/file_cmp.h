#include <stdio.h>

#include "cmp.h" //NOLINT

enum cmp_result file_cmp(FILE *f1, FILE *f2, size_t sz);
